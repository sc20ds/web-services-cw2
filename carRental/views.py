from datetime import datetime
import json
from django import template
from django.http import HttpResponse, HttpResponseBadRequest
import requests
from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect, render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer

from .models import Car, CarBooking, CarModel, Currency, Location
from .serializers import CarSerializer
from django.db.models import Q
from django.core.cache import cache
from dateutil import parser

from django.conf import settings



def rent(request, start_date, end_date, car_id):
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')

    if CarBooking.objects.filter(car_id=car_id, start_date=start_date, end_date=end_date).first() == None:
        car_booking = CarBooking.objects.create(
            car_id=car_id,
            start_date=start_date,
            end_date=end_date
        )
        return render(request, "carRental/success.html", {
            "booking_id": car_booking.id
        })
    return redirect('/')

def index(request):
    return render(request, 'carRental/index.html', {} )

def add_car(request):
    if request.method == 'POST':
        type = request.POST.get('type')
        engine = request.POST.get('engine')
        price = request.POST.get('price')
        
        manufacturer = request.POST.get('manufacturer')
        model_name = request.POST.get('model_name')
        number_of_seats = request.POST.get('number_of_seats')

        location_name = request.POST.get('location')
        currency_code = request.POST.get('currency_code')

        currency = Currency.objects.filter(code=currency_code).first()

        car_model = CarModel.objects.filter(
            manufacturer = manufacturer,
            model_name = model_name,
            number_of_seats = number_of_seats
        ).first()
        if not car_model:
            car_model = CarModel.objects.create(
                manufacturer = manufacturer,
                model_name = model_name,
                number_of_seats = number_of_seats
            )

        location = Location.objects.filter(
            city_name = location_name
        ).first()

        if not location:
            location = Location.objects.create(
                airport_code = None,
                city_name = location_name,
                airport_name = None
            )

        car = Car.objects.create(
            type = type,
            engine = engine,
            price = price,
            currency = currency,
            model = car_model,
            location = location
        )

        return redirect('/cars')
    
    context = {
        "types": [x['type'] for x in Car.objects.values('type').distinct()],
        "engines": [x['engine'] for x in Car.objects.values('engine').distinct()],
        "currencies": Currency.objects.all()
    } 
    return render(request, 'carRental/addCar.html', context)

def search(request):
    if request.method == 'POST':
        city_id = request.POST.get('city')
        start_date = request.POST.get('start_date')
        end_date = request.POST.get('end_date')
        price_min = request.POST.get('price_min')
        price_max = request.POST.get('price_max')
        number_of_seats = request.POST.get('number_of_seats')
        car_type = request.POST.get('type')

        cars = Car.objects.filter(location__id=city_id)
        if price_min:
            cars = cars.filter(price__gte=price_min)
        
        if price_max:
            cars = cars.filter(price__lte=price_max)

        if number_of_seats:
            cars = cars.filter(model__num_of_seats=number_of_seats)

        type = request.GET.get('type')
        if type and type != "I don't mind":
            cars = cars.filter(type=type)
        
        
        cars = cars.filter(
            Q(carbooking__end_date__lt=start_date) | Q(carbooking__start_date__gt=end_date)
        )

        current_timestamp = timezone.now()
        filtered_cars = []
        lock_duration_seconds = getattr(settings, 'LOCK_DURATION_SECONDS', 1200)

        for car in cars:
            car_lock_key = f'car_lock_{car.id}'
            lock_details = cache.get(car_lock_key)

            if lock_details and (current_timestamp - lock_details['timestamp']).total_seconds() < lock_duration_seconds:
                start_date = lock_details['start_date']
                end_date = lock_details['end_date']

                # Check if the lock collides with any car booking
                if CarBooking.objects.filter(
                    car=car,
                    start_date__lte=end_date,
                    end_date__gte=start_date
                ).exists():
                    continue

            filtered_cars.append(car)

        context = {
            "cars": filtered_cars,
            "start_date": start_date,
            "end_date": end_date,
        }
        
        return render(request, 'carRental/carList.html', context = context)



    cities = Location.objects.all()
    car_types = ["I don't mind"] + [x['type'] for x in Car.objects.values('type').distinct()] 
    context = {
        "cities": cities,
        "car_types": car_types,
    }

    return render(request, 'carRental/search.html', context = context )



def cars(request):
    context = {
        "cars": Car.objects.all(),
    }
    return render(request, 'carRental/carList.html', context = context)


def booking_lookup(request):
    if request.method == 'POST':
        booking_id = request.POST.get('booking_id')
        booking = get_object_or_404(CarBooking, id=booking_id)
        return render(request, 'carRental/booking_details.html', {'booking': booking})
    
    return render(request, 'carRental/booking_lookup.html')


class CarAPIView(APIView):
    def get(self, request):
        location_name = request.GET.get('location')
        if not location_name:
            return Response({"error": "'location' parameter is required."}, status=status.HTTP_400_BAD_REQUEST)

        cars = Car.objects.filter(
            Q(location__airport_code=location_name) |
            Q(location__city_name=location_name) |
            Q(location__airport_name=location_name)
        )

        start_time = request.GET.get('start_time')
        if not start_time:
            return Response({"error": "'start_time' parameter is required."}, status=status.HTTP_400_BAD_REQUEST)

        end_time = request.GET.get('end_time')
        if not end_time:
            return Response({"error": "'end_time' parameter is required."}, status=status.HTTP_400_BAD_REQUEST)


        price_min = request.GET.get('price_min')
        if price_min:
            cars = cars.filter(price__gte=price_min)
        
        price_max = request.GET.get('price_max')
        if price_max:
            cars = cars.filter(price__lte=price_max)

        num_of_seats = request.GET.get('num_of_seats')
        if num_of_seats:
            cars = cars.filter(model__number_of_seats=num_of_seats)

        type = request.GET.get('type')
        if type:
            cars = cars.filter(type=type)
            
        engine = request.GET.get('engine')
        if engine:
            cars = cars.filter(engine=engine)

        model_name = request.GET.get('model_name')
        if model_name:
            cars = cars.filter(model__model_name=model_name)


        try:
            start_time = datetime.strptime(start_time, '%Y-%m-%d')
        except ValueError:
            try:
                start_time = datetime.fromisoformat(start_time)
                start_time = start_time.date()
            except ValueError:
                try:
                    start_time = parser.parse(start_time)
                    start_time = start_time.date()
                except:
                    return HttpResponseBadRequest('Invalid date format')
        try:
            end_time = datetime.strptime(end_time, '%Y-%m-%d')
        except ValueError:
            try:
                end_time = datetime.fromisoformat(end_time)
                end_time = end_time.date()
            except ValueError:
                try:
                    end_time = parser.parse(end_time)
                    end_time = end_time.date()
                except:
                    return HttpResponseBadRequest('Invalid date format')
            
        cars = cars.exclude(
            Q(carbooking__start_date__lte=end_time, carbooking__end_date__gte=start_time)
        )


        current_timestamp = timezone.now()
        filtered_cars = []
        lock_duration_seconds = getattr(settings, 'LOCK_DURATION_SECONDS', 1200)

        for car in cars:
            car_lock_key = f'car_lock_{car.id}'
            lock_details = cache.get(car_lock_key)

            if lock_details and (current_timestamp - lock_details['timestamp']).total_seconds() < lock_duration_seconds:
                start_date = lock_details['start_date']
                end_date = lock_details['end_date']

                # Check if the lock collides with any car booking
                if CarBooking.objects.filter(
                    car=car,
                    start_date__lte=end_date,
                    end_date__gte=start_date
                ).exists():
                    continue

            filtered_cars.append(car)

        filtered_cars = Car.objects.filter(id__in = [x.id for x in filtered_cars])

        serializer = CarSerializer(filtered_cars, many=True)
        json_data = json.loads(JSONRenderer().render(serializer.data).decode('utf-8'))
        return Response(json_data, status=status.HTTP_200_OK, content_type='application/json')


class CarAPIDetail(APIView):
    def get(self, request, car_id):
        if not car_id:
            return Response({"error": "'car_id' parameter is required."}, status=status.HTTP_400_BAD_REQUEST)

        car = get_object_or_404(Car, id=car_id)

        serializer = CarSerializer(car, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)


class LockCarAPIView(APIView):
    def post(self, request):
        data = request.data
        required_parameters = ['car_id', 'start_date', 'end_date']

        # Check if all required parameters are present
        for param in required_parameters:
            if param not in data:
                error_message = f'Missing parameter: {param}'
                return Response({'message': error_message}, status=status.HTTP_400_BAD_REQUEST)

        car_id = data['car_id']
        start_time = data['start_date']
        end_time = data['end_date']


        try:
            start_time = datetime.strptime(start_time, '%Y-%m-%d')
        except ValueError:
            try:
                start_time = datetime.fromisoformat(start_time)
                start_time = start_time.date()
            except ValueError:
                try:
                    start_time = parser.parse(start_time)
                    start_time = start_time.date()
                except:
                    return HttpResponseBadRequest('Invalid date format')
        try:
            end_time = datetime.strptime(end_time, '%Y-%m-%d')
        except ValueError:
            try:
                end_time = datetime.fromisoformat(end_time)
                end_time = end_time.date()
            except ValueError:
                try:
                    end_time = parser.parse(end_time)
                    end_time = end_time.date()
                except:
                    return HttpResponseBadRequest('Invalid date format')
            

        # Check if the car is already locked
        car_lock_key = f'car_lock_{car_id}'
        lock_details = cache.get(car_lock_key)
        if lock_details:
            lock_timestamp = lock_details.get('timestamp')
            current_timestamp = timezone.now()

            # Check if the lock has expired (using the LOCK_DURATION_SECONDS setting)
            lock_duration_seconds = getattr(settings, 'LOCK_DURATION_SECONDS', 1200)
            if (current_timestamp - lock_timestamp).total_seconds() < lock_duration_seconds:
                return Response({'message': 'Car is already locked'}, status=status.HTTP_403_FORBIDDEN)
            else:
                # Remove the expired lock from the cache
                cache.delete(car_lock_key)

        # Lock the car by storing the lock details in cache
        lock_details = {
            'timestamp': timezone.now(),
            'start_date': start_time,
            'end_date': end_time
        }
        cache.set(car_lock_key, lock_details)

        return Response({'message': 'Car locked successfully'}, status=status.HTTP_200_OK)

class BookCarAPIView(APIView):
    def post(self, request):
        data = request.data

        required_parameters = ['car_id', 'start_date', 'end_date', 'psp_id', 'psp_checkout_id']

        # Check if all required parameters are present
        for param in required_parameters:
            if param not in data:
                error_message = f'Missing parameter: {param}'
                return Response({'message': error_message}, status=status.HTTP_400_BAD_REQUEST)

        car_id = data['car_id']
        start_time = data['start_date']
        end_time = data['end_date']
        psp_checkout_id = data['psp_checkout_id']

        try:
            start_time = datetime.strptime(start_time, '%Y-%m-%d')
        except ValueError:
            try:
                start_time = datetime.fromisoformat(start_time)
                start_time = start_time.date()
            except ValueError:
                try:
                    start_time = parser.parse(start_time)
                    start_time = start_time.date()
                except:
                    return HttpResponseBadRequest('Invalid date format')
        try:
            end_time = datetime.strptime(end_time, '%Y-%m-%d')
        except ValueError:
            try:
                end_time = datetime.fromisoformat(end_time)
                end_time = end_time.date()
            except ValueError:
                try:
                    end_time = parser.parse(end_time)
                    end_time = end_time.date()
                except:
                    return HttpResponseBadRequest('Invalid date format')
            

        try:
            psp_id = int(data['psp_id'])
        except:
            return Response({'message': 'PSP must be an integer'}, status=status.HTTP_400_BAD_REQUEST)


        psp_urls = getattr(settings, 'PAYMENT_SERVICE_PROVIDER_URLS', dict())
        if psp_id not in psp_urls.keys():
            return Response({'message': 'Invalid PSP'}, status=status.HTTP_400_BAD_REQUEST)

        psp_url = psp_urls[psp_id]
        # Construct the URL to query
        checkout_status_url = f"{psp_url}/api/checkout/{psp_checkout_id}/status"

        # Send a GET request to the PSP endpoint
        response = requests.get(checkout_status_url)

        if response.status_code == 200:
            # Add an entry to the CarBooking table
            car_booking = CarBooking.objects.create(
                car_id=car_id,
                start_date=start_time,
                end_date=end_time
            )
            return Response({'message': 'Car booked successfully'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Failed to book the car'}, status=status.HTTP_400_BAD_REQUEST)