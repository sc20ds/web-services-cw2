import json
from django.test import TestCase
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from django.urls import reverse
from datetime import date
from .models import Car, CarBooking, CarModel, Currency, Location
from .views import CarAPIDetail, CarAPIView
from faker import Faker

faker = Faker()

from rest_framework import status
from rest_framework.test import APITestCase


def populate_currency():
    currency_codes = ['AED', 'ARS', 'AUD', 'BDT', 'BGN', 'BHD', 'BND', 'BOB', 'BRL', 'BWP', 'BYN', 'CAD', 'CHF', 'CLP', 'CNY', 'COP', 'CRC', 'CZK', 'DKK', 'DOP', 'DZD', 'EGP', 'EUR', 'FJD', 'GBP', 'GEL', 'GHS', 'HKD', 'HRK', 'HUF', 'IDR', 'ILS', 'INR', 'IQD', 'JOD', 'JPY', 'KES', 'KRW', 'KWD', 'KZT', 'LBP', 'LKR', 'LTL', 'MAD', 'MMK', 'MOP', 'MUR', 'MXN', 'MYR', 'NAD', 'NGN', 'NIO', 'NOK', 'NPR', 'NZD', 'OMR', 'PEN', 'PHP', 'PKR', 'PLN', 'PYG', 'QAR', 'RON', 'RSD', 'RUB', 'SAR', 'SEK', 'SGD', 'SVC', 'THB', 'TND', 'TRY', 'TWD', 'TZS', 'UAH', 'UGX', 'USD', 'UYU', 'UZS', 'VEF', 'VES', 'VND', 'XOF', 'ZAR']
    currency_names = ['United Arab Emirates Dirham', 'Argentine Peso', 'Australian Dollar', 'Bangladeshi Taka', 'Bulgarian Lev', 'Bahraini Dinar', 'Brunei Dollar', 'Bolivian Boliviano', 'Brazilian Real', 'Botswanan Pula', 'Belarusian Ruble', 'Canadian Dollar', 'Swiss Franc', 'Chilean Peso', 'Chinese Yuan', 'Colombian Peso', 'Costa Rican Colón', 'Czech Koruna', 'Danish Krone', 'Dominican Peso', 'Algerian Dinar', 'Egyptian Pound', 'Euro','Fijian Dollar','British Pound Sterling','Georgian Lari','Ghanaian Cedi','Hong Kong Dollar','Croatian Kuna','Hungarian Forint','Indonesian Rupiah','Israeli New Sheqel','Indian Rupee','Iraqi Dinar','Jordanian Dinar','Japanese Yen','Kenyan Shilling','South Korean Won','Kuwaiti Dinar','Kazakhstani Tenge','Lebanese Pound','Sri Lankan Rupee','Lithuanian Litas','Moroccan Dirham','Myanma Kyat','Macanese Pataca','Mauritian Rupee','Mexican Peso','Malaysian Ringgit','Namibian Dollar','Nigerian Naira','Nicaraguan Córdoba','Norwegian Krone','Nepalese Rupee','New Zealand Dollar','Omani Rial','Peruvian Nuevo Sol','Philippine Peso','Pakistani Rupee','Polish Zloty','Paraguayan Guarani','Qatari Rial','Romanian Leu','Serbian Dinar','Russian Ruble','Saudi Riyal','Swedish Krona','Singapore Dollar','Salvadoran Colón','Thai Baht','Tunisian Dinar','Turkish Lira','New Taiwan Dollar','Tanzanian Shilling','Ukrainian Hryvnia','Ugandan Shilling','US Dollar','Uruguayan Peso','Uzbekistan Som','Venezuelan Bolívar (2008-2018)','Venezuelan Bolívar','Vietnamese Dong','CFA Franc BCEAO','South African Rand']
    for currency_code, currency_name in zip(currency_codes, currency_names):
        Currency.objects.create(name=currency_name, code=currency_code)


def populate_car_models():
    makes = ['Ford', 'Toyota', 'Chevrolet', 'Honda', 'Nissan', 'BMW', 'Mercedes-Benz', 'Audi',
                'Volkswagen', 'Hyundai', 'Kia', 'Subaru', 'Mazda', 'Lexus', 'Jeep', 'Volvo', 'Tesla',
                'Land Rover', 'Mitsubishi', 'Jaguar']
    
    models = ['Fiesta', 'Corolla', 'Cruze', 'Civic', 'Altima', '3 Series', 'C-Class', 'A4',
                'Golf', 'Elantra', 'Optima', 'Outback', 'CX-5', 'RX', 'Wrangler', 'XC90', 'Model S',
                'Range Rover', 'Outlander', 'F-PACE']

    for _ in range(20):
        manufacturer=faker.random_element(elements=makes)
        model_name=faker.random_element(elements=models)
        number_of_seats = faker.random_int(min=2, max=8)
        CarModel.objects.create(manufacturer=manufacturer, model_name=model_name, number_of_seats=number_of_seats)


def populate_locations():
    airport_names = [
        "Poznań-Ławica Airport",
        "John Paul II International Airport Kraków-Balice",
        "Warsaw Chopin Airport",
        "Wrocław-Copernicus Airport",
        "Gdańsk Lech Wałęsa Airport",
        "Berlin Brandenburg Airport",
        "Charles de Gaulle Airport",
        "Amsterdam Airport Schiphol",
        "Heathrow Airport",
        "Barcelona-El Prat Airport",
        "Vienna International Airport",
        "Athens International Airport",
        "Budapest Ferenc Liszt International Airport",
        "Leonardo da Vinci–Fiumicino Airport",
        "Zurich Airport"
    ]

    cities = [
        "Poznań",
        "Kraków",
        "Warsaw",
        "Wrocław",
        "Gdańsk",
        "Berlin",
        "Paris",
        "Amsterdam",
        "London",
        "Barcelona",
        "Vienna",
        "Athens",
        "Budapest",
        "Rome",
        "Zurich"
    ]

    airport_codes = [
        "POZ",
        "KRK",
        "WAW",
        "WRO",
        "GDN",
        "BER",
        "CDG",
        "AMS",
        "LHR",
        "BCN",
        "VIE",
        "ATH",
        "BUD",
        "FCO",
        "ZRH"
    ]

    for airport_name, city_name, airport_code  in zip(airport_names, cities, airport_codes):
        location = Location(
            airport_code=airport_code,
            city_name=city_name,
            airport_name=airport_name
            )
        location.save()
            
def populate_cars():
    currencies = Currency.objects.all()
    car_models = CarModel.objects.all()
    locations = Location.objects.all()
    types = ["manual", "automatic"]
    engines = ["petrol", "hybrid", "electric"]

    for _ in range(500):
        car_type = faker.random_element(types)
        engine = faker.random_element(engines)
        price = faker.random_int(min=1000, max=5000)
        currency = faker.random_element(currencies)
        car_model = faker.random_element(car_models)
        location = faker.random_element(locations)

        Car.objects.create(type=car_type, engine=engine, price=price, currency=currency,
                            model=car_model, location=location)
        
def populate_car_bookings():
    cars = Car.objects.all()

    for _ in range(30):
        car = faker.random_element(cars)
        start_date = faker.date_between(start_date='-30d', end_date='today')
        end_date = faker.date_between(start_date=start_date, end_date='+30d')

        CarBooking.objects.create(car=car, start_date=start_date, end_date=end_date)



def json_contains(expected, actual):
    if isinstance(expected, dict) and isinstance(actual, dict):
        for key, value in expected.items():
            if key not in actual or not json_contains(value, actual[key]):
                return False
        return True
    elif isinstance(expected, list) and isinstance(actual, list):
        for item in expected:
            found = False
            for itemA in actual:
                if item == itemA or json_contains(item, itemA):
                    found = True

            if not found:
                return False
        return True
    else:
        return expected == actual

class CarAPITest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

        populate_currency()
        populate_car_models()
        populate_locations()
        populate_cars()
        populate_car_bookings()
        
        manufacturer = 'Audi'
        model_name = 'A4'
        number_of_seats = 4
        location_name = 'Piaseczno'
        
        car_model = CarModel.objects.filter(
            manufacturer = manufacturer,
            model_name = model_name,
            number_of_seats = number_of_seats
        ).first()
        if not car_model:
            car_model = CarModel.objects.create(
                manufacturer = manufacturer,
                model_name = model_name,
                number_of_seats = number_of_seats
            )

        location = Location.objects.filter(
            city_name = location_name
        ).first()

        if not location:
            location = Location.objects.create(
                airport_code = None,
                city_name = location_name,
                airport_name = None
            )
        Car.objects.create(
            type = 'manual',
            engine = 'automatic',
            price = 1231,
            currency = Currency.objects.filter(code = 'GBP').first(),
            model = car_model,
            location = location
        )
        
    def test_car_api_view(self):
        url = 'api/cars'
        query_params = {
            'location': 'Piaseczno',
            'start_time': '2023-05-05',
            'end_time': '2023-06-06',
            'price_min': 12,
            'num_of_seats': 4,
            'type': 'manual',
            'engine': 'automatic',
            'model_name': 'A4'
        }
        request = self.factory.get(url, query_params)
        response = CarAPIView.as_view()(request)
        self.assertEqual(response.status_code, 200)

        expected_data = [
            {
                'type': 'manual',
                'engine': 'automatic',
                'price': 1231,
                'model': {
                    "manufacturer": "Audi",
                    "model_name": "A4",
                    "number_of_seats": 4
                },
                "currency": { "name": "British Pound Sterling", "code": "GBP" },
                "location": {
                    "city_name": "Piaseczno"
                }
            }
        ]
        expected_dict = json.loads(json.dumps(expected_data))
        actual_dict = json.loads(json.dumps(response.data))

        is_contained = json_contains(expected_dict, actual_dict)
        assert is_contained


    def test_car_api_detail(self):
        url = '/api/car/501'
        query_params = {}
        request = self.factory.get(url, query_params)
        response = CarAPIDetail.as_view()(request, car_id=501)


        self.assertEqual(response.status_code, 200)

        expected_data = {
                'type': 'manual',
                'engine': 'automatic',
                'price': 1231,
                'model': {
                    "manufacturer": "Audi",
                    "model_name": "A4",
                    "number_of_seats": 4
                },
                "currency": { "name": "British Pound Sterling", "code": "GBP" },
                "location": {
                    "city_name": "Piaseczno"
                }
            }

        expected_dict = json.loads(json.dumps(expected_data))
        actual_dict = json.loads(json.dumps(response.data))
        is_contained = json_contains(expected_dict, actual_dict)
        assert is_contained

    def test_car_api_detail(self):
        url = '/api/car/501'
        query_params = {}
        request = self.factory.get(url, query_params)
        response = CarAPIDetail.as_view()(request, car_id=501)


        self.assertEqual(response.status_code, 200)

        expected_data = {
                'type': 'manual',
                'engine': 'automatic',
                'price': 1231,
                'model': {
                    "manufacturer": "Audi",
                    "model_name": "A4",
                    "number_of_seats": 4
                },
                "currency": { "name": "British Pound Sterling", "code": "GBP" },
                "location": {
                    "city_name": "Piaseczno"
                }
            }

        expected_dict = json.loads(json.dumps(expected_data))
        actual_dict = json.loads(json.dumps(response.data))
        is_contained = json_contains(expected_dict, actual_dict)
        assert is_contained


class LockCarAPIViewTestCase(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

        populate_currency()
        populate_car_models()
        populate_locations()
        populate_cars()
        populate_car_bookings()
        
        manufacturer = 'Audi'
        model_name = 'A4'
        number_of_seats = 4
        location_name = 'Piaseczno'
        
        car_model = CarModel.objects.filter(
            manufacturer = manufacturer,
            model_name = model_name,
            number_of_seats = number_of_seats
        ).first()
        if not car_model:
            car_model = CarModel.objects.create(
                manufacturer = manufacturer,
                model_name = model_name,
                number_of_seats = number_of_seats
            )

        location = Location.objects.filter(
            city_name = location_name
        ).first()

        if not location:
            location = Location.objects.create(
                airport_code = None,
                city_name = location_name,
                airport_name = None
            )
        Car.objects.create(
            type = 'manual',
            engine = 'automatic',
            price = 1231,
            currency = Currency.objects.filter(code = 'GBP').first(),
            model = car_model,
            location = location
        )
    def test_lock_car_successfully(self):
        url = '/api/lock'
        data = {
            'car_id': 104,
            'start_date': '2023-05-10',
            'end_date': '2023-05-20'
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'message': 'Car locked successfully'})

    def test_missing_parameters(self):
        url = '/api/lock'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10'
            # Missing 'end_date' parameter
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {'message': 'Missing parameter: end_date'}
        )

    def test_car_already_locked(self):
        url = '/api/lock'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10',
            'end_date': '2023-05-20'
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'message': 'Car locked successfully'})

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'message': 'Car is already locked'})



from unittest.mock import patch

class BookCarAPIViewTestCase(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

        populate_currency()
        populate_car_models()
        populate_locations()
        populate_cars()
        populate_car_bookings()
        
        manufacturer = 'Audi'
        model_name = 'A4'
        number_of_seats = 4
        location_name = 'Piaseczno'
        
        car_model = CarModel.objects.filter(
            manufacturer = manufacturer,
            model_name = model_name,
            number_of_seats = number_of_seats
        ).first()
        if not car_model:
            car_model = CarModel.objects.create(
                manufacturer = manufacturer,
                model_name = model_name,
                number_of_seats = number_of_seats
            )

        location = Location.objects.filter(
            city_name = location_name
        ).first()

        if not location:
            location = Location.objects.create(
                airport_code = None,
                city_name = location_name,
                airport_name = None
            )
        Car.objects.create(
            type = 'manual',
            engine = 'automatic',
            price = 1231,
            currency = Currency.objects.filter(code = 'GBP').first(),
            model = car_model,
            location = location
        )
    def test_book_car_successfully(self):
        url = '/api/book'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10',
            'end_date': '2023-05-20',
            'psp_id': 1,
            'psp_checkout_id': 'checkout_123'
        }

        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 200

            response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'message': 'Car booked successfully'})

    def test_missing_parameters(self):
        url = '/api/book'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10',
            'end_date': '2023-05-20',
            # Missing 'psp_id' and 'psp_checkout_id' parameters
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {'message': 'Missing parameter: psp_id'}
        )

    def test_invalid_psp(self):
        url = '/api/book'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10',
            'end_date': '2023-05-20',
            'psp_id': 999,  # Invalid PSP ID
            'psp_checkout_id': 'checkout_123'
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'message': 'Invalid PSP'})

    def test_failed_booking(self):
        url = '/api/book'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10',
            'end_date': '2023-05-20',
            'psp_id': 1,
            'psp_checkout_id': 'checkout_123'
        }

        # Mock the PSP endpoint response
        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 400

            response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'message': 'Failed to book the car'})

    def test_invalid_date_format(self):
        url = '/api/book'
        data = {
            'car_id': 123,
            'start_date': '2023-05-10',
            'end_date': 'invalid-date-format',  # Invalid date format
            'psp_id': 1,
            'psp_checkout_id': 'checkout_123'
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
