from django.db import models
from django.utils import timezone
import datetime


class Currency(models.Model):
    name = models.CharField(max_length=200, null=False)
    code = models.CharField(max_length=5, null=False)
    
    def __str__(self):
        return f'<Currency: {self.name}>'


class CarModel(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    number_of_seats = models.IntegerField()
    
class Location(models.Model):
    airport_code = models.CharField(max_length=6, null=True)
    city_name = models.CharField(max_length=6)
    airport_name = models.CharField(max_length=1000, null=True)

class Car(models.Model):
    type = models.CharField(max_length=30, null=False) # manual / automatic
    engine = models.CharField(max_length=30, null=False) # petrol / electric / hybrid
    price = models.IntegerField(null=False)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    
class CarBooking(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
