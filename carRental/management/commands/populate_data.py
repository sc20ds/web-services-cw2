from django.core.management.base import BaseCommand
from faker import Faker

from carRental.models import Currency, CarModel, Location, Car, CarBooking
faker = Faker()


class Command(BaseCommand):
    help = 'Populate database with random records'

    def handle(self, *args, **options):
        self.populate_currency()
        self.populate_car_models()
        self.populate_locations()
        self.populate_cars()
        self.populate_car_bookings()

    def populate_currency(self):
        currency_codes = ['AED', 'ARS', 'AUD', 'BDT', 'BGN', 'BHD', 'BND', 'BOB', 'BRL', 'BWP', 'BYN', 'CAD', 'CHF', 'CLP', 'CNY', 'COP', 'CRC', 'CZK', 'DKK', 'DOP', 'DZD', 'EGP', 'EUR', 'FJD', 'GBP', 'GEL', 'GHS', 'HKD', 'HRK', 'HUF', 'IDR', 'ILS', 'INR', 'IQD', 'JOD', 'JPY', 'KES', 'KRW', 'KWD', 'KZT', 'LBP', 'LKR', 'LTL', 'MAD', 'MMK', 'MOP', 'MUR', 'MXN', 'MYR', 'NAD', 'NGN', 'NIO', 'NOK', 'NPR', 'NZD', 'OMR', 'PEN', 'PHP', 'PKR', 'PLN', 'PYG', 'QAR', 'RON', 'RSD', 'RUB', 'SAR', 'SEK', 'SGD', 'SVC', 'THB', 'TND', 'TRY', 'TWD', 'TZS', 'UAH', 'UGX', 'USD', 'UYU', 'UZS', 'VEF', 'VES', 'VND', 'XOF', 'ZAR']
        currency_names = ['United Arab Emirates Dirham', 'Argentine Peso', 'Australian Dollar', 'Bangladeshi Taka', 'Bulgarian Lev', 'Bahraini Dinar', 'Brunei Dollar', 'Bolivian Boliviano', 'Brazilian Real', 'Botswanan Pula', 'Belarusian Ruble', 'Canadian Dollar', 'Swiss Franc', 'Chilean Peso', 'Chinese Yuan', 'Colombian Peso', 'Costa Rican Colón', 'Czech Koruna', 'Danish Krone', 'Dominican Peso', 'Algerian Dinar', 'Egyptian Pound', 'Euro','Fijian Dollar','British Pound Sterling','Georgian Lari','Ghanaian Cedi','Hong Kong Dollar','Croatian Kuna','Hungarian Forint','Indonesian Rupiah','Israeli New Sheqel','Indian Rupee','Iraqi Dinar','Jordanian Dinar','Japanese Yen','Kenyan Shilling','South Korean Won','Kuwaiti Dinar','Kazakhstani Tenge','Lebanese Pound','Sri Lankan Rupee','Lithuanian Litas','Moroccan Dirham','Myanma Kyat','Macanese Pataca','Mauritian Rupee','Mexican Peso','Malaysian Ringgit','Namibian Dollar','Nigerian Naira','Nicaraguan Córdoba','Norwegian Krone','Nepalese Rupee','New Zealand Dollar','Omani Rial','Peruvian Nuevo Sol','Philippine Peso','Pakistani Rupee','Polish Zloty','Paraguayan Guarani','Qatari Rial','Romanian Leu','Serbian Dinar','Russian Ruble','Saudi Riyal','Swedish Krona','Singapore Dollar','Salvadoran Colón','Thai Baht','Tunisian Dinar','Turkish Lira','New Taiwan Dollar','Tanzanian Shilling','Ukrainian Hryvnia','Ugandan Shilling','US Dollar','Uruguayan Peso','Uzbekistan Som','Venezuelan Bolívar (2008-2018)','Venezuelan Bolívar','Vietnamese Dong','CFA Franc BCEAO','South African Rand']
        for currency_code, currency_name in zip(currency_codes, currency_names):
            Currency.objects.create(name=currency_name, code=currency_code)



    def populate_car_models(self):
        makes = ['Ford', 'Toyota', 'Chevrolet', 'Honda', 'Nissan', 'BMW', 'Mercedes-Benz', 'Audi',
                 'Volkswagen', 'Hyundai', 'Kia', 'Subaru', 'Mazda', 'Lexus', 'Jeep', 'Volvo', 'Tesla',
                 'Land Rover', 'Mitsubishi', 'Jaguar']
        
        models = ['Fiesta', 'Corolla', 'Cruze', 'Civic', 'Altima', '3 Series', 'C-Class', 'A4',
                  'Golf', 'Elantra', 'Optima', 'Outback', 'CX-5', 'RX', 'Wrangler', 'XC90', 'Model S',
                  'Range Rover', 'Outlander', 'F-PACE']

        for _ in range(20):
            manufacturer=faker.random_element(elements=makes)
            model_name=faker.random_element(elements=models)
            number_of_seats = faker.random_int(min=2, max=8)
            CarModel.objects.create(manufacturer=manufacturer, model_name=model_name, number_of_seats=number_of_seats)

    def populate_locations(self):
        fake = Faker()

        airport_names = [
            "Poznań-Ławica Airport",
            "John Paul II International Airport Kraków-Balice",
            "Warsaw Chopin Airport",
            "Wrocław-Copernicus Airport",
            "Gdańsk Lech Wałęsa Airport",
            "Berlin Brandenburg Airport",
            "Charles de Gaulle Airport",
            "Amsterdam Airport Schiphol",
            "Heathrow Airport",
            "Barcelona-El Prat Airport",
            "Vienna International Airport",
            "Athens International Airport",
            "Budapest Ferenc Liszt International Airport",
            "Leonardo da Vinci–Fiumicino Airport",
            "Zurich Airport"
        ]

        cities = [
            "Poznań",
            "Kraków",
            "Warsaw",
            "Wrocław",
            "Gdańsk",
            "Berlin",
            "Paris",
            "Amsterdam",
            "London",
            "Barcelona",
            "Vienna",
            "Athens",
            "Budapest",
            "Rome",
            "Zurich"
        ]

        airport_codes = [
            "POZ",
            "KRK",
            "WAW",
            "WRO",
            "GDN",
            "BER",
            "CDG",
            "AMS",
            "LHR",
            "BCN",
            "VIE",
            "ATH",
            "BUD",
            "FCO",
            "ZRH"
        ]

        for airport_name, city_name, airport_code  in zip(airport_names, cities, airport_codes):
            location = Location(
                airport_code=airport_code,
                city_name=city_name,
                airport_name=airport_name
                )
            location.save()


    def populate_cars(self):
        currencies = Currency.objects.all()
        car_models = CarModel.objects.all()
        locations = Location.objects.all()
        types = ["manual", "automatic"]
        engines = ["petrol", "hybrid", "electric"]

        for _ in range(500):
            car_type = faker.random_element(types)
            engine = faker.random_element(engines)
            price = faker.random_int(min=1000, max=5000)
            currency = faker.random_element(currencies)
            car_model = faker.random_element(car_models)
            location = faker.random_element(locations)

            Car.objects.create(type=car_type, engine=engine, price=price, currency=currency,
                               model=car_model, location=location)

    def populate_car_bookings(self):
        cars = Car.objects.all()

        for _ in range(30):
            car = faker.random_element(cars)
            start_date = faker.date_between(start_date='-30d', end_date='today')
            end_date = faker.date_between(start_date=start_date, end_date='+30d')

            CarBooking.objects.create(car=car, start_date=start_date, end_date=end_date)
