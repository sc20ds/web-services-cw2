from rest_framework import serializers
from .models import Currency, CarModel, Location, Car, CarBooking


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarModel
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'


class CarSerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()
    model = CarModelSerializer()
    location = LocationSerializer()

    class Meta:
        model = Car
        fields = '__all__'


class CarBookingSerializer(serializers.ModelSerializer):
    car = CarSerializer()

    class Meta:
        model = CarBooking
        fields = '__all__'
