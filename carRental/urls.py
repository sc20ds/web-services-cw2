from django.urls import path

from . import views

app_name = "carRental"

from django.urls import path
from .views import BookCarAPIView, CarAPIDetail, CarAPIView, LockCarAPIView

urlpatterns = [
    path('api/cars', CarAPIView.as_view(), name='car_list'),
    path('api/car/<int:car_id>', CarAPIDetail.as_view(), name='car_detail'),
    path('api/lock', LockCarAPIView.as_view(), name='lock'),
    path('api/book', BookCarAPIView.as_view(), name='book'),
    path('', views.index, name='index'),
    path('search', views.search, name='search'),
    path('lookup', views.booking_lookup, name='booking_lookup'),
    path('rent/<str:start_date>/<str:end_date>/<int:car_id>', views.rent, name='rent'),
    path('cars', views.cars, name='cars'),
    path('addCar', views.add_car, name='addCar'),
]
