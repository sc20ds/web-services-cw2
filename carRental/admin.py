from django.contrib import admin


from .models import Currency, CarModel, Location, Car, CarBooking

admin.site.register(Currency)
admin.site.register(CarModel)
admin.site.register(Location)
admin.site.register(Car)
admin.site.register(CarBooking)