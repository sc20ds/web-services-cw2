# web-services-cw2

## Deployment and passwords:
The name of your pythonanywhere domain: http://sc20ds.pythonanywhere.com/
The password I have to use to login to my admin account on your service:

login: ammar
password: ammar123

## Getting started

This is a Django project for managing car rentals.

Instructions to run on Linux:

<code>python3 -m venv venv # creates Virtual Environment</code>
<code>source venv/bin/activate # activates the Virtual Environement</code>
<code>pip3 install -r requirements.txt # installs all necessary dependencies</code>
<code>python3 manage.py makemigrations # creates database migrations if neccessary</code>
<code>python3 manage.py migrate #  migrates if neccessary</code>
<code>python3 manage.py populate_data #  populates the database with items</code>
<code>python3 manage.py runserver #  starts the application</code>


The application will be accessible on the local machine on port 8000 by default

The api is available on "/api" path
the frontend is avaialble on "/" path

## Example requests
The example requests and responses can be found in <code>requests</code> file


## Frontend 

Since each service is independent, and my service provides car rental service to the aggregator, it is reasonable (and in fact a real-world scenario) to have a website for the service. On top of providing customers from the aggregator with the services, the Car Rental service allows the users to book the car independently of the flights, directly through my website. For this reason, there is a frontend for my application. 

The frontend is accessible using the link below:
http://sc20ds.pythonanywhere.com/





